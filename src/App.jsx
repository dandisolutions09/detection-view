// import { useState } from "react";
// import reactLogo from "./assets/react.svg";
// import viteLogo from "/vite.svg";
// import "./App.css";

// function App() {
//   const [count, setCount] = useState(0);
//   const [pp, setPP] = useState("");
//   const [hsp, setHsP] = useState("");

//   function handleChange(e) {
//     // Access the file object
//     const file = e.target.files[0];
//     setPP(file);
//     console.log("upload profile picture");

//   }

//   return (
//     <>
//       <div className="flex flex-row gap-3 border-b bod border-gray-400 py-3">
//         <label className="text-black">Profile</label>
//         <input
//           type="file"
//           onChange={handleChange}
//           className="mt-[20%] cursor-pointer"
//         />
//       </div>

//       <img
//         src={!pp ? hsp : pp}
//         className="sm:w-[150px] w-[70px] sm:h-[150px] h-[70px] rounded-full object-cover"
//       />
//     </>
//   );
// }

// export default App;

import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import axios from "axios";
import { useRef } from "react";
import { PacmanLoader } from "react-spinners";

function App() {
  const [count, setCount] = useState(0);
  const [pp, setPP] = useState(null);
  const [hsp, setHsP] = useState("https://via.placeholder.com/150");
  const [fileState, setFileState] = useState();
  const ref = useRef(0);

  const [processedImage, setProcessedImage] = useState();
  const [loading, setLoading] = useState(false);

  //setProcessedImage

  function handleChange(e) {
    // Access the file object

    const file = e.target.files[0];

    console.log(typeof file);
    //<class '_io.BufferedReader'>

    handleUploadImage(file);

    setFileState(file);
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        setPP(reader.result);
      };
      reader.readAsDataURL(file);
      console.log("Profile picture uploaded");
    }
  }

  const handleUploadImage = async (file) => {
    try {
      setLoading(true);
      const formData = new FormData();
      formData.append("file", file);

      // Replace 'your-upload-endpoint' with your actual upload endpoint
      const response = await fetch("https://server-apple-detection.onrender.com/upload/", {
        method: "POST",
        body: formData,
        headers: {
          // "Authorization": `Bearer ${stateToken}`,
        },
      });

      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }

      const responseData = await response.json();
      console.log("Upload successful:", responseData.image_path);

      setProcessedImage(responseData.image_path);
      // setPP(responseData.image_url);
      // console.log("ID", selectedRowData._id);
      // handleUpdateProfilePicURL(responseData.image_url);
    } catch (error) {
      // console.error("Error uploading file:", error);
      if (error.response && error.response.status !== 200) {
        console.error("Error uploading file:", error);
      } else {
        console.log("Upload successful:", error.response);
      }
    } finally {
      console.log("Request completed."); // This will be executed regardless of success or failure
      setLoading(false);
    }
  };

  // Function to convert ArrayBuffer to Base64
  function arrayBufferToBase64(buffer) {
    let binary = "";
    const bytes = new Uint8Array(buffer);
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return btoa(binary);
  }

  return (
    <>
      <div className="mt-2 flex flex-col gap-4">
        {/* <label className="text-black">Profile</label> */}
        <input
          type="file"
          onChange={handleChange}
          className="mt-[20%] cursor-pointer"
        />

        {/* <img
          src={processedImage ? processedImage : pp}
          alt="Profile"
          height={300}
          width={300}
          // className="sm:w-[150px] w-[70px] sm:h-[150px] h-[70px] rounded-full object-cover"
        />

        {loading && <PacmanLoader size={65} color="#36d7b7" />} */}
      </div>

      <div style={{ position: "relative", width: 300, height: 300 }}>
        <img
          src={processedImage ? processedImage : pp}
          alt="Profile"
          height={300}
          width={300}
          // className="sm:w-[150px] w-[70px] sm:h-[150px] h-[70px] rounded-full object-cover"
          style={{
            width: "100%",
            height: "100%",
            objectFit: "cover",
            // borderRadius: "50%",
          }}
        />

        {loading && (
          <div
            style={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
          >
            <PacmanLoader size={35} color="#000" />
          </div>
        )}
      </div>

      {/* <button onClick={handleUploadImage}>Upload</button> */}

      {/* <img
        src={processedImage || hsp}
        alt="Profile"
        height={300}
        width={300}
        // className="sm:w-[150px] w-[70px] sm:h-[150px] h-[70px] rounded-full object-cover"
      /> */}
    </>
  );
}

export default App;
